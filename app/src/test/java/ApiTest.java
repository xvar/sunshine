import android.arch.core.executor.testing.InstantTaskExecutorRule;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.haiminov.sunshine.model.OpenWeatherMapApi;
import com.haiminov.sunshine.model.dto.WeatherResponseDTO;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.squareup.okhttp.mockwebserver.MockResponse;
import com.squareup.okhttp.mockwebserver.MockWebServer;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Map;

import io.reactivex.observers.TestObserver;
import okio.BufferedSource;
import okio.Okio;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@RunWith(JUnit4.class)
public class ApiTest {

    private MockWebServer mockWebServer;
    private OpenWeatherMapApi apiInterface;
    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();
    //http://api.openweathermap.org/data/2.5/forecast/daily?q=Moscow&mode=json&units=metric&cnt=14&appid=75596f8686eb03bf0df64a209b4795c4

    @Before
    public void setUp() throws Exception {
        mockWebServer = new MockWebServer();
        final Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        final GsonConverterFactory gsonFactory = GsonConverterFactory.create(gson);
        apiInterface = new Retrofit.Builder()
                .baseUrl(String.valueOf(mockWebServer.url("/")))
                .addConverterFactory(gsonFactory)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(OpenWeatherMapApi.class);
    }

    @After
    public void stopService() throws IOException {
        mockWebServer.shutdown();
    }

    @Test
    public void testOkResponse() throws IOException {
        enqueueResponse("response.json");

        TestObserver<WeatherResponseDTO> testObserver =
                apiInterface.weatherObservable("Moscow", "json", "metric", 14, "75596f8686eb03bf0df64a209b4795c4")
                .test();

        testObserver.assertNoErrors();
        testObserver.assertValueCount(1);

        testObserver.assertValueAt(0, responseDTO -> "Moscow".equals(responseDTO.getCity().getName()));
        testObserver.assertValueAt(0, responseDTO -> responseDTO.getList().size() == 14);
        testObserver.assertValueAt(0, responseDTO -> responseDTO.getList().get(0).getDt() == 1520240400L);
    }

    private void enqueueResponse(String fileName) throws IOException {
        enqueueResponse(fileName, Collections.emptyMap());
    }

    private void enqueueResponse(String fileName, Map<String, String> headers) throws IOException {
        InputStream inputStream = getClass().getClassLoader()
                .getResourceAsStream("json/" + fileName);
        BufferedSource source = Okio.buffer(Okio.source(inputStream));
        MockResponse mockResponse = new MockResponse();
        for (Map.Entry<String, String> header : headers.entrySet()) {
            mockResponse.addHeader(header.getKey(), header.getValue());
        }
        mockWebServer.enqueue(mockResponse
                .setBody(source.readString(StandardCharsets.UTF_8)));
    }

}
