package com.haiminov.sunshine;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.haiminov.sunshine.data.WeatherContract;
import com.haiminov.sunshine.di.Injectable;
import com.haiminov.sunshine.di.SunshineViewModelFactory;
import com.haiminov.sunshine.viewmodel.ForecastViewModel;
import com.haiminov.sunshine.model.OpenWeatherMapApi;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;


/**
 * Created by Alex on 28.09.2014.
 */
public class ForecastFragment extends Fragment implements Injectable {

    private ForecastViewModel viewModel;
    private ForecastAdapter mForecastAdapter;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    OpenWeatherMapApi api;
    @Inject
    SunshineViewModelFactory factory;
    private RecyclerView mRecyclerView;
    private ProgressBar mProgressBar;

    public interface Callback {
        void onItemSelected(Uri dateUri);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.forecastfragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_refresh) {
            updateWeather();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean mUseTodayLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mForecastAdapter = new ForecastAdapter(getActivity(), (forecast, position) -> {
            final Activity activity = getActivity();
            if (activity != null) {
                String locationSetting = Utility.getPreferredLocation(activity);
                Uri weatherUri = WeatherContract.WeatherEntry.buildWeatherLocationWithDate(
                        locationSetting, forecast.getStartDate());
                ((Callback) getActivity()).onItemSelected(weatherUri);
            }
        });

        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerview_forecast);
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progress_bar);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mRecyclerView.setAdapter(mForecastAdapter);

        viewModel = ViewModelProviders.of(this, factory).get(ForecastViewModel.class);

        mForecastAdapter.setUseTodayLayout(mUseTodayLayout);
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        Disposable viewSubs = viewModel.getViewDataObservable().subscribe(viewData -> {
            switch (viewData.getState()) {
                case ForecastContract.State.DATA:
                    if (viewData.getItems() == null) {
                        showError(R.string.error_no_items);
                        //todo fancy log
                        return;
                    }
                    showProgress(false);
                    mForecastAdapter.updateItems(viewData.getItems());
                    break;
                case ForecastContract.State.ERROR:
                    showError(R.string.error_connection);
                    showProgress(false);
                    break;
                case ForecastContract.State.LOADING:
                    showProgress(true);
                    break;
            }
        });
        compositeDisposable.add(viewSubs);
    }

    private void showProgress(boolean isShown) {
        int progressVisibility = isShown ? View.VISIBLE : View.GONE;
        int recyclerVisibility = !isShown ? View.VISIBLE : View.GONE;
        mRecyclerView.setVisibility(recyclerVisibility);
        mProgressBar.setVisibility(progressVisibility);
    }

    private void showError(@NonNull @StringRes int errorId) {
        Toast.makeText(getActivity(), errorId, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStop() {
        super.onStop();
        compositeDisposable.clear();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        updateWeather();
    }

    void onLocationChanged( ) {
        updateWeather();
    }

    private void updateWeather() {
        viewModel.query();
    }

    public void setUseTodayLayout(boolean useTodayLayout) {
        mUseTodayLayout = useTodayLayout;
        if (mForecastAdapter != null) {
            mForecastAdapter.setUseTodayLayout(mUseTodayLayout);
        }
    }
}
