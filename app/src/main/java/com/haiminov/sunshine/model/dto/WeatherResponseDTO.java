package com.haiminov.sunshine.model.dto;

import com.haiminov.sunshine.model.dto.City;
import com.haiminov.sunshine.model.dto.ForecastDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 25.09.2016.
 */

public class WeatherResponseDTO {
    private List<ForecastDTO> list = new ArrayList<>();
    private City city;

    public List<ForecastDTO> getList() {
        return list;
    }

    public City getCity() {
        return city;
    }

    public void setList(List<ForecastDTO> list) {
        this.list = list;
    }

    public void setCity(City city) {
        this.city = city;
    }
}
