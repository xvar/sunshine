package com.haiminov.sunshine.model;

import com.haiminov.sunshine.model.dto.WeatherResponseDTO;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.functions.Function;

public class DayForecastMapper implements Function<WeatherResponseDTO, List<DayForecast>> {

    @Inject
    public DayForecastMapper() {}

    @Override
    public List<DayForecast> apply(WeatherResponseDTO responseDTO) throws Exception {
        if (responseDTO == null) {
            return null;
        }
        return WeatherModel.from(responseDTO).getList();
    }
}
