package com.haiminov.sunshine.model;

import com.haiminov.sunshine.model.dto.WeatherResponseDTO;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Alex on 25.09.2016.
 */

public interface OpenWeatherMapApi {
    String BASE_URL = "http://api.openweathermap.org/data/2.5/forecast/";
    @GET("daily")
    Call<WeatherResponseDTO> listWeather(
            @Query("q") String query,
            @Query("mode") String format,
            @Query("units") String units,
            @Query("cnt") int numDays,
            @Query("appid") String appid
    );

    @GET("daily")
    Observable<WeatherResponseDTO> weatherObservable(
            @Query("q") String query,
            @Query("mode") String format,
            @Query("units") String units,
            @Query("cnt") int numDays,
            @Query("appid") String appid
    );

}
