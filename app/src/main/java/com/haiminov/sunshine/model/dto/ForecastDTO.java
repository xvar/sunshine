package com.haiminov.sunshine.model.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 25.09.2016.
 */
public class ForecastDTO {
    private long dt;
    private double pressure;
    private int humidity;
    private double speed;
    private double deg;
    private List<Weather> weather = new ArrayList<>();
    private Temperature temp;

    public double getPressure() {
        return pressure;
    }

    public int getHumidity() {
        return humidity;
    }

    public double getSpeed() {
        return speed;
    }

    public double getDeg() {
        return deg;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public Temperature getTemp() {
        return temp;
    }

    public void setPressure(double pressure) {
        this.pressure = pressure;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public void setDeg(double deg) {
        this.deg = deg;
    }

    public void setWeather(List<Weather> weather) {
        this.weather = weather;
    }

    public void setTemp(Temperature temp) {
        this.temp = temp;
    }

    public long getDt() {
        return dt;
    }

    public void setDt(long dt) {
        this.dt = dt;
    }
}
