package com.haiminov.sunshine.model;

import android.net.Uri;
import android.support.annotation.NonNull;

import io.reactivex.Maybe;
import io.reactivex.Observable;

public interface DetailModel {
    Observable<Uri> getUriObservable();
    Maybe<DayForecast> getDayForecast(@NonNull Uri uri);
    void updateUri(@NonNull Uri uri);

    //todo refactor to lifecycle provider
    void onStart();
    void onStop();
}