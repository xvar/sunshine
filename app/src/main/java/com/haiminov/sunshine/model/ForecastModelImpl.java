package com.haiminov.sunshine.model;

import android.support.annotation.NonNull;

import com.haiminov.sunshine.ForecastContract;
import com.haiminov.sunshine.SunshineApp;
import com.haiminov.sunshine.data.WeatherProvider;
import com.haiminov.sunshine.model.dto.WeatherResponseDTO;

import javax.inject.Inject;

import io.reactivex.Observable;

public class ForecastModelImpl implements ForecastContract.Model {

    @Inject
    public ForecastModelImpl(@NonNull OpenWeatherMapApi api) {
        this.api = api;
    }

    @NonNull
    private final OpenWeatherMapApi api;

    @Override
    public Observable<WeatherResponseDTO> getWeather(String query, String format, String units, int numDays, String appid) {
        return api.weatherObservable(query, format, units, numDays, appid);
    }

    @Override
    public void save(@NonNull WeatherResponseDTO data, @NonNull String query) {
        WeatherProvider.saveToDb(SunshineApp.getAppContext(), data, query);
    }
}
