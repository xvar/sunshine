package com.haiminov.sunshine.model;

import android.database.ContentObserver;
import android.databinding.BindingAdapter;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.widget.ImageView;

import com.haiminov.sunshine.SunshineApp;
import com.haiminov.sunshine.Utility;
import com.haiminov.sunshine.data.WeatherProvider;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.subjects.ReplaySubject;

public class DetailModelImpl implements DetailModel {

    @Inject
    public DetailModelImpl() {}

    private ReplaySubject<Uri> subject = ReplaySubject.createWithSize(1);
    private ContentObserver contentObserver = new ContentObserver(new Handler(Looper.getMainLooper())) {
        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            updateUri(subject.getValue());
        }
    };

    @Override
    public Observable<Uri> getUriObservable() {
        return subject;
    }

    @Override
    public Maybe<DayForecast> getDayForecast(@NonNull Uri uri) {
        return Maybe.fromCallable(() -> WeatherProvider.getForecastByUri(SunshineApp.getAppContext(), uri));
    }

    @Override
    public void updateUri(@NonNull Uri uri) {
        subject.onNext(uri);
        unRegisterObserver();
        registerObserver();
    }

    @Override
    public void onStart() {
        registerObserver();
    }

    private void registerObserver() {
        Uri value = subject.getValue();
        if (value == null) {
            return;
        }
        SunshineApp.getAppContext().getContentResolver().registerContentObserver(subject.getValue(), false, contentObserver);
    }

    @Override
    public void onStop() {
        unRegisterObserver();
    }

    private void unRegisterObserver() {
        SunshineApp.getAppContext().getContentResolver().unregisterContentObserver(contentObserver);
    }

    @BindingAdapter("artRes")
    public static void loadArtResource(@NonNull ImageView imageView, int resId) {
        if (resId <= 0) {
            return;
        }
        imageView.setImageResource(Utility.getArtResourceForWeatherCondition(resId));
    }
}
