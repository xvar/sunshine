package com.haiminov.sunshine.model.dto;

/**
 * Created by Alex on 25.09.2016.
 */
public class Weather {
    private int id;
    private String main;

    public String getDescription() {
        return main;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setMain(String main) {
        this.main = main;
    }
}
