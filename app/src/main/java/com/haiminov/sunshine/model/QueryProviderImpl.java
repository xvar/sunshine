package com.haiminov.sunshine.model;

import android.support.annotation.NonNull;

import com.haiminov.sunshine.ForecastContract;
import com.haiminov.sunshine.SunshineApp;
import com.haiminov.sunshine.Utility;

public class QueryProviderImpl implements ForecastContract.QueryProvider {
    @NonNull
    @Override
    public String getQuery() {
        return Utility.getPreferredLocation(SunshineApp.getAppContext());
    }

    @NonNull
    @Override
    public String getFormat() {
        return "json";
    }

    @Override
    public int getNumDays() {
        return 14;
    }

    @NonNull
    @Override
    public String getUnits() {
        return "metric";
    }

    @NonNull
    @Override
    public String getAppId() {
        return "75596f8686eb03bf0df64a209b4795c4";
    }
}
