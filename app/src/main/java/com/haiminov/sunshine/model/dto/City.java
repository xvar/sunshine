package com.haiminov.sunshine.model.dto;

/**
 * Created by Alex on 25.09.2016.
 */
public class City {
    private long id;
    private String name;
    private LatLngDTO coord;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public LatLngDTO getCoord() {
        return coord;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCoord(LatLngDTO coord) {
        this.coord = coord;
    }
}
