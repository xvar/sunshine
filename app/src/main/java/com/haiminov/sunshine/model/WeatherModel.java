package com.haiminov.sunshine.model;

import android.support.annotation.NonNull;

import com.haiminov.sunshine.model.dto.City;
import com.haiminov.sunshine.model.dto.WeatherResponseDTO;

import java.util.ArrayList;
import java.util.List;

public class WeatherModel {
    private List<DayForecast> list = new ArrayList<>();
    private City city;

    static WeatherModel from (@NonNull WeatherResponseDTO responseDTO) {
        WeatherModel model = new WeatherModel();
        model.setCity(responseDTO.getCity());
        model.setList(DayForecast.from(responseDTO.getList()));
        return model;
    }

    public List<DayForecast> getList() {
        return list;
    }

    public City getCity() {
        return city;
    }

    public void setList(List<DayForecast> list) {
        this.list = list;
    }

    public void setCity(City city) {
        this.city = city;
    }
}
