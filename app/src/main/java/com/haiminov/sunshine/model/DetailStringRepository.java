package com.haiminov.sunshine.model;

import android.support.annotation.NonNull;

public interface DetailStringRepository {
    @NonNull
    String getShareForecastString(long dateMillis);

    @NonNull
    String getDateText(long dateInMillis);

    @NonNull
    String getMonthDayText(long dateInMillis);

    @NonNull
    String getTemperatureText(double temperature);

    @NonNull
    String getHumidityText(double humidity);

    @NonNull
    String getPressureText(double pressure);

    @NonNull
    String getWindText(float speed, float degrees);
}
