package com.haiminov.sunshine.model;

import android.support.annotation.NonNull;

import com.haiminov.sunshine.Utility;
import com.haiminov.sunshine.model.dto.ForecastDTO;
import com.haiminov.sunshine.model.dto.Temperature;

import java.util.ArrayList;
import java.util.List;

public class DayForecast {

    private int weatherId;
    @NonNull
    private String description;
    @NonNull
    private Temperature temperature;
    private final int humidity;
    private final double pressure;
    private final double speed;
    private double windDegrees;
    private long dateInMillis = 0;

    public DayForecast(
            int weatherId,
            @NonNull String description,
            @NonNull Temperature temperature,
            int humidity,
            double pressure,
            double speed,
            double windDegrees
    ) {
        this.weatherId = weatherId;
        this.description = description;
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;
        this.speed = speed;
        this.windDegrees = windDegrees;
    }

    public static List<DayForecast> from(@NonNull List<ForecastDTO> list) {
        List<DayForecast> res = new ArrayList<>(list.size());
        for (int i = 0; i < list.size(); i++) {
            final DayForecast forecast = from(list.get(i));
            forecast.setStartDate(Utility.getDayTimestamp(i));
            res.add(forecast);
        }
        return res;
    }

    private static DayForecast from(ForecastDTO dto) {
        return new DayForecast(
                dto.getWeather().get(0).getId(),
                dto.getWeather().get(0).getDescription(),
                dto.getTemp(),
                dto.getHumidity(),
                dto.getPressure(),
                dto.getSpeed(),
                dto.getDeg()
        );
    }

    public int getWeatherId() {
        return weatherId;
    }

    public void setWeatherId(int weatherId) {
        this.weatherId = weatherId;
    }

    @NonNull
    public String getDescription() {
        return description;
    }

    public void setDescription(@NonNull String description) {
        this.description = description;
    }

    public Temperature getTemperature() {
        return temperature;
    }

    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
    }

    public long getStartDate() {
        return dateInMillis;
    }

    public void setStartDate(long dateInMillis) {
        this.dateInMillis = dateInMillis;
    }

    public int getHumidity() {
        return humidity;
    }

    public double getPressure() {
        return pressure;
    }

    public double getSpeed() {
        return speed;
    }

    public double getWindDegrees() {
        return windDegrees;
    }
}
