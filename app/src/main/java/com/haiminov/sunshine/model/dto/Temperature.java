package com.haiminov.sunshine.model.dto;

/**
 * Created by Alex on 25.09.2016.
 */
public class Temperature {
    private double min;
    private double max;

    public double getMin() {
        return min;
    }

    public double getMax() {
        return max;
    }

    public void setMin(double min) {
        this.min = min;
    }

    public void setMax(double max) {
        this.max = max;
    }
}
