package com.haiminov.sunshine.model.dto;

/**
 * Created by Alex on 25.09.2016.
 */
public class LatLngDTO {
    private double lat;
    private double lng;

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
