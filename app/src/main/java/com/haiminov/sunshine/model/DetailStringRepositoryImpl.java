package com.haiminov.sunshine.model;

import android.support.annotation.NonNull;

import com.haiminov.sunshine.R;
import com.haiminov.sunshine.SunshineApp;
import com.haiminov.sunshine.Utility;

import javax.inject.Inject;

public class DetailStringRepositoryImpl implements DetailStringRepository {

    @Inject
    public DetailStringRepositoryImpl() {}

    @NonNull
    @Override
    public String getShareForecastString(long dateMillis) {
        return Utility.getFormattedMonthDay(SunshineApp.getAppContext(), dateMillis);
    }

    @NonNull
    @Override
    public String getDateText(long dateInMillis) {
        return Utility.getDayName(SunshineApp.getAppContext(), dateInMillis);
    }

    @NonNull
    @Override
    public String getMonthDayText(long dateInMillis) {
        return Utility.getFormattedMonthDay(SunshineApp.getAppContext(), dateInMillis);
    }

    @NonNull
    @Override
    public String getTemperatureText(double temperature) {
        boolean isMetric = Utility.isMetric(SunshineApp.getAppContext());
        return Utility.formatTemperature(SunshineApp.getAppContext(), temperature, isMetric);
    }

    @NonNull
    @Override
    public String getHumidityText(double humidity) {
        return SunshineApp.getAppContext().getResources().getString(R.string.format_humidity, humidity);
    }

    @NonNull
    @Override
    public String getPressureText(double pressure) {
        return SunshineApp.getAppContext().getResources().getString(R.string.format_pressure, pressure);
    }

    @NonNull
    @Override
    public String getWindText(float speed, float degrees) {
        return Utility.getFormattedWind(SunshineApp.getAppContext(), speed, degrees);
    }
}
