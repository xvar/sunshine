package com.haiminov.sunshine.di;

import android.arch.lifecycle.ViewModel;

import com.haiminov.sunshine.ForecastContract;
import com.haiminov.sunshine.model.DetailModel;
import com.haiminov.sunshine.model.DetailModelImpl;
import com.haiminov.sunshine.model.ForecastModelImpl;
import com.haiminov.sunshine.model.OpenWeatherMapApi;
import com.haiminov.sunshine.viewmodel.DetailViewModel;
import com.haiminov.sunshine.viewmodel.ForecastViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {
    @Binds
    @IntoMap
    @DI.ViewModelKey(ForecastViewModel.class)
    abstract ViewModel forecastViewModel(ForecastViewModel forecastViewModel);

    @Binds
    @IntoMap
    @DI.ViewModelKey(DetailViewModel.class)
    abstract ViewModel groupViewModel(DetailViewModel detailViewModel);
}
