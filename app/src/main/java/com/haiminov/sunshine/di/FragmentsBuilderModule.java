package com.haiminov.sunshine.di;

import com.haiminov.sunshine.DetailFragment;
import com.haiminov.sunshine.ForecastFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentsBuilderModule {
    @ContributesAndroidInjector
    abstract ForecastFragment contributeForecastFragment();

    @ContributesAndroidInjector
    abstract DetailFragment contributeDetailFragment();
}
