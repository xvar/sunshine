package com.haiminov.sunshine.di;

import com.haiminov.sunshine.model.OpenWeatherMapApi;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = OkHttpClientModule.class)
public class NetworkModule {
    @Singleton
    @Provides
    public OpenWeatherMapApi openWeatherMapApi(Retrofit retrofit) {
        return retrofit.create(OpenWeatherMapApi.class);
    }

    @Singleton
    @Provides
    public Retrofit retrofit(
            OkHttpClient client,
            GsonConverterFactory gsonConverterFactory,
            RxJava2CallAdapterFactory rxJava2CallAdapterFactory
    ) {
        return new Retrofit.Builder()
                .client(client)
                .baseUrl(OpenWeatherMapApi.BASE_URL)
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(rxJava2CallAdapterFactory)
                .build();
    }

    @Singleton
    @Provides
    public GsonConverterFactory gsonConverterFactory() {
        return GsonConverterFactory.create();
    }

    @Singleton
    @Provides
    public RxJava2CallAdapterFactory rxJava2CallAdapterFactory() {
        return RxJava2CallAdapterFactory.create();
    }

    @Provides
    @Singleton
    @DI.UiThread
    Scheduler provideSchedulerUI() {
        return AndroidSchedulers.mainThread();
    }

    @Provides
    @Singleton
    @DI.WorkerThread
    Scheduler provideSchedulerIO() {
        return Schedulers.io();
    }
}
