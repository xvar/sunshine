package com.haiminov.sunshine.di;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppContextModule {
    private Context context;

    public AppContextModule(Context context) {
        this.context = context;
    }

    @Singleton
    @Provides
    @DI.ApplicationContext
    public Context getContext() {
        return context.getApplicationContext();
    }
}
