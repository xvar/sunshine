package com.haiminov.sunshine.di;

import com.haiminov.sunshine.ForecastContract;
import com.haiminov.sunshine.model.DetailModel;
import com.haiminov.sunshine.model.DetailModelImpl;
import com.haiminov.sunshine.model.DetailStringRepository;
import com.haiminov.sunshine.model.DetailStringRepositoryImpl;
import com.haiminov.sunshine.model.ForecastModelImpl;
import com.haiminov.sunshine.model.OpenWeatherMapApi;
import com.haiminov.sunshine.model.QueryProviderImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ModelModule {
    @Singleton
    @Provides
    public DetailModel detailModel() {
        return new DetailModelImpl();
    }

    @Provides
    public ForecastContract.Model forecastModel(OpenWeatherMapApi api) {
        return new ForecastModelImpl(api);
    }

    @Provides
    public ForecastContract.QueryProvider queryProvider() {
        return new QueryProviderImpl();
    }

    @Singleton
    @Provides
    public DetailStringRepository detailStringRepository() {
        return new DetailStringRepositoryImpl();
    }
}
