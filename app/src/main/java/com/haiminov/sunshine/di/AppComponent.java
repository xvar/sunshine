package com.haiminov.sunshine.di;

import android.app.Application;

import com.haiminov.sunshine.SunshineApp;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Singleton
@Component(modules = {
        AppContextModule.class, AppModule.class, AndroidInjectionModule.class,
        FragmentsBuilderModule.class, ViewModelModule.class
})
public interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance Builder application(Application application);
        Builder appContextModule(AppContextModule module);
        AppComponent build();
    }
    void inject(SunshineApp githubApp);
}
