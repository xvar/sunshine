package com.haiminov.sunshine.di;

import android.arch.lifecycle.ViewModel;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.inject.Qualifier;
import javax.inject.Scope;

import dagger.MapKey;

public interface DI {
    @Qualifier
    @interface ApplicationContext {}

    @Qualifier
    @interface UiThread {}

    @Qualifier
    @interface WorkerThread {}

    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.RUNTIME)
    @MapKey
    @interface ViewModelKey {
        Class<? extends ViewModel> value();
    }

    @Scope
    @Retention(RetentionPolicy.RUNTIME)
    @interface ViewModelScope { }
}
