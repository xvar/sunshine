package com.haiminov.sunshine;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.haiminov.sunshine.model.DayForecast;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ViewHolder> {

    private static final int VIEW_TYPE_TODAY = 0;
    private static final int VIEW_TYPE_FUTURE_DAY = 1;
    private static final int VIEW_TYPE_COUNT = 2;
    @NonNull
    private final Context context;
    @NonNull
    private final OnItemClickListener itemClickListener;
    private List<DayForecast> items = new ArrayList<>();

    private boolean mUseTodayLayout = true;

    public interface OnItemClickListener {
        void onItemClicked(@NonNull DayForecast DayForecast, int position);
    }

    public ForecastAdapter(@NonNull Context context, @NonNull OnItemClickListener itemClickListener) {
        this.context = context;
        this.itemClickListener = itemClickListener;
    }

    public void updateItems(@NonNull Collection<DayForecast> items) {
        this.items.clear();
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    public void setUseTodayLayout(boolean useTodayLayout) {
        mUseTodayLayout = useTodayLayout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layoutId = (viewType == VIEW_TYPE_TODAY) ? R.layout.list_item_forecast_today
                : R.layout.list_item_forecast;

        View v = LayoutInflater.from(context).inflate(layoutId, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (items.size() > position && items.get(position) != null) {
            bindView(holder, items.get(position), getItemViewType(position), position);
        }
    }

    private void bindView(@NonNull ViewHolder holder, @NonNull DayForecast forecast, int viewType, int position) {
        holder.itemView.setOnClickListener(v -> itemClickListener.onItemClicked(forecast, position));
        switch (viewType) {
            case VIEW_TYPE_TODAY: {
                holder.iconView.setImageResource(
                        Utility.getArtResourceForWeatherCondition(forecast.getWeatherId())
                );
                break;
            }
            case VIEW_TYPE_FUTURE_DAY: {
                holder.iconView.setImageResource(
                        Utility.getIconResourceForWeatherCondition(forecast.getWeatherId())
                );
                break;
            }
        }


        holder.dateView.setText(Utility.getFriendlyDayString(context, forecast.getStartDate()));

        final String description = forecast.getDescription();
        holder.descriptionView.setText(description);
        holder.iconView.setContentDescription(description);

        boolean isMetric = Utility.isMetric(context);
        double high = forecast.getTemperature().getMax();
        holder.highTempView.setText(Utility.formatTemperature(context, high, isMetric));

        double low = forecast.getTemperature().getMin();
        holder.lowTempView.setText(Utility.formatTemperature(context, low, isMetric));
    }

    @Override
    public int getItemViewType(int position) {
        return (position == 0 && mUseTodayLayout) ? VIEW_TYPE_TODAY : VIEW_TYPE_FUTURE_DAY;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    /**
     * Cache of the children views for a DayForecast list item.
     */
    static class ViewHolder extends RecyclerView.ViewHolder {
        final ImageView iconView;
        final TextView dateView;
        final TextView descriptionView;
        final TextView highTempView;
        final TextView lowTempView;

        ViewHolder(View view) {
            super(view);
            iconView = (ImageView) view.findViewById(R.id.list_item_icon);
            dateView = (TextView) view.findViewById(R.id.list_item_date_textview);
            descriptionView = (TextView) view.findViewById(R.id.list_item_forecast_textview);
            highTempView = (TextView) view.findViewById(R.id.list_item_high_textview);
            lowTempView = (TextView) view.findViewById(R.id.list_item_low_textview);
        }
    }
}