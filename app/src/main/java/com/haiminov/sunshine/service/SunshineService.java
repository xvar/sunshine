package com.haiminov.sunshine.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.haiminov.sunshine.Utility;
import com.haiminov.sunshine.data.WeatherProvider;
import com.haiminov.sunshine.model.OpenWeatherMapApi;
import com.haiminov.sunshine.model.dto.WeatherResponseDTO;

import java.io.IOException;

import javax.inject.Inject;

import dagger.android.DaggerIntentService;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by KhaA on 09.04.2015.
 */
public class SunshineService extends DaggerIntentService {
    public static final String LOCATION_QUERY_EXTRA = "lqe";
    public static final String SUNSHINE_SERVICE_NAME = "SunshineSeRvice";
    private final String LOG_TAG = SunshineService.class.getSimpleName();
    @Inject
    OpenWeatherMapApi service;

    public SunshineService() {
        super(SUNSHINE_SERVICE_NAME);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d("debug", "onhandleIntent");
        String locationQuery = intent.getStringExtra(LOCATION_QUERY_EXTRA);

        String format = "json";
        String units = "metric";
        int numDays = 14;

        Call<WeatherResponseDTO> list = service.listWeather(
                locationQuery,
                format,
                Utility.isMetric(this) ? "metric" : "imperial",
                numDays,
                "75596f8686eb03bf0df64a209b4795c4"
        );
        try {
            Response<WeatherResponseDTO> response = list.execute();
            if (response.isSuccessful()) {
                WeatherResponseDTO model = response.body();
                WeatherProvider.saveToDb(this, model, locationQuery);
            }
        } catch (IOException e) {
            Log.e("exception", e.toString());
        }

        return;
    }


    static public class AlarmReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Intent sendIntent = new Intent(context, SunshineService.class);
            sendIntent.putExtra(SunshineService.LOCATION_QUERY_EXTRA, intent.getStringExtra(SunshineService.LOCATION_QUERY_EXTRA));
            context.startService(sendIntent);
        }
    }
}