package com.haiminov.sunshine.viewmodel;

import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.haiminov.sunshine.model.DayForecast;
import com.haiminov.sunshine.model.DetailModel;
import com.haiminov.sunshine.model.DetailStringRepository;

import javax.inject.Inject;

import io.reactivex.ObservableSource;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;

public class DetailViewModel extends ViewModel {

    public final ObservableField<String> forecastStr = new ObservableField<>();
    public final ObservableInt weatherId = new ObservableInt();
    public final ObservableField<String> date = new ObservableField<>();
    public final ObservableField<String> month = new ObservableField<>();
    public final ObservableField<String> minTemperature = new ObservableField<>();
    public final ObservableField<String> maxTemperature = new ObservableField<>();
    public final ObservableField<String> humidity = new ObservableField<>();
    public final ObservableField<String> pressure = new ObservableField<>();
    public final ObservableField<String> wind = new ObservableField<>();
    public final ObservableField<String> description = new ObservableField<>();

    @NonNull
    private DetailModel detailModel;
    private DetailStringRepository stringRepository;
    private Disposable disposable;

    @Inject
    public DetailViewModel(@NonNull DetailModel detailModel, @NonNull DetailStringRepository stringRepository) {
        this.detailModel = detailModel;
        this.stringRepository = stringRepository;
    }

    public void init() {
        disposable = detailModel.getUriObservable()
                .switchMap((Function<Uri, ObservableSource<DayForecast>>) uri -> detailModel.getDayForecast(uri).toObservable())
                .subscribe(this::updateBindings);
    }

    private void updateBindings(@NonNull DayForecast forecast) {
        final long startDate = forecast.getStartDate();
        final String dateText = stringRepository.getShareForecastString(startDate);
        forecastStr.set(String.format("%s - %s - %s/%s", dateText, forecast.getDescription(), forecast.getTemperature().getMax(), forecast.getTemperature().getMin()));
        weatherId.set(forecast.getWeatherId());
        date.set(stringRepository.getDateText(startDate));
        month.set(stringRepository.getMonthDayText(startDate));
        minTemperature.set(stringRepository.getTemperatureText(forecast.getTemperature().getMin()));
        maxTemperature.set(stringRepository.getTemperatureText(forecast.getTemperature().getMax()));
        humidity.set(stringRepository.getHumidityText(forecast.getHumidity()));
        pressure.set(stringRepository.getPressureText(forecast.getPressure()));
        wind.set(stringRepository.getWindText((float) forecast.getSpeed(), (float) forecast.getWindDegrees()));
        description.set(forecast.getDescription());
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
    }
    
}
