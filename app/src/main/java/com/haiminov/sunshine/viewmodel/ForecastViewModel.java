package com.haiminov.sunshine.viewmodel;

import android.arch.lifecycle.ViewModel;
import android.util.Log;

import com.haiminov.sunshine.ForecastContract;
import com.haiminov.sunshine.model.DayForecastMapper;

import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.ReplaySubject;

public class ForecastViewModel extends ViewModel {

    private ForecastContract.QueryProvider queryProvider;
    //composite disposable not needed (it's here just to free resources quicker)
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    DayForecastMapper mapper;
    private ForecastContract.Model model;

    @Inject
    public ForecastViewModel(@NonNull ForecastContract.Model model, @NonNull ForecastContract.QueryProvider queryProvider) {
        this.model = model;
        this.queryProvider = queryProvider;
    }

    private ReplaySubject<ForecastContract.ViewData> viewSubject = ReplaySubject.createWithSize(1);

    public Observable<ForecastContract.ViewData> getViewDataObservable() {
        return viewSubject;
    }

    public void query() {
        viewSubject.onNext(new ForecastContract.ViewData(ForecastContract.State.LOADING, null));
        final String query = queryProvider.getQuery();
        final Disposable disposable = model.getWeather(
                query,
                queryProvider.getFormat(),
                queryProvider.getUnits(),
                queryProvider.getNumDays(),
                queryProvider.getAppId()
        )
                .doOnNext(responseDTO -> model.save(responseDTO, query))
                .map(mapper)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    weatherModel -> {
                        viewSubject.onNext(new ForecastContract.ViewData(ForecastContract.State.DATA, weatherModel));
                    },
                    throwable -> {
                        viewSubject.onNext(new ForecastContract.ViewData(ForecastContract.State.ERROR, null));
                        Log.e("sunshine", "error getting response", throwable);
                    }
                );
        compositeDisposable.add(disposable);
    }

    @Override
    protected void onCleared() {
        compositeDisposable.clear();
        super.onCleared();
    }
}
