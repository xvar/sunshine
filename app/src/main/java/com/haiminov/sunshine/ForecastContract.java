package com.haiminov.sunshine;

import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.haiminov.sunshine.model.DayForecast;
import com.haiminov.sunshine.model.dto.WeatherResponseDTO;

import java.lang.annotation.Retention;
import java.util.List;

import io.reactivex.Observable;

import static com.haiminov.sunshine.ForecastContract.State.DATA;
import static com.haiminov.sunshine.ForecastContract.State.ERROR;
import static com.haiminov.sunshine.ForecastContract.State.LOADING;
import static java.lang.annotation.RetentionPolicy.SOURCE;

public interface ForecastContract {

    @Retention(SOURCE)
    @IntDef({LOADING, DATA, ERROR})
    @interface State {
        int LOADING = 0;
        int DATA = 1;
        int ERROR = 2;
    }

    class ViewData {
        private @State int state;
        @Nullable
        private List<DayForecast> list;

        public ViewData(int state, @Nullable List<DayForecast> list) {
            this.state = state;
            this.list = list;
        }

        @State
        public int getState() {
            return state;
        }

        @Nullable
        public List<DayForecast> getItems() {
            return list;
        }
    }

    interface QueryProvider {
        @NonNull
        String getQuery();
        /* currently - json */
        @NonNull
        String getFormat();
        int getNumDays();
        /*"imperial" or "metric"*/
        @NonNull
        String getUnits();
        @NonNull
        String getAppId();
    }

    interface Model {
        Observable<WeatherResponseDTO> getWeather(String query, String format, String units, int numDays, String appid);
        void save(@NonNull WeatherResponseDTO data, @NonNull String query);
    }

}
