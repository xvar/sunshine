package com.haiminov.sunshine;

/**
 * Created by Alex on 03.03.2015.
 */

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.ShareActionProvider;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.haiminov.sunshine.data.WeatherContract;
import com.haiminov.sunshine.data.WeatherContract.WeatherEntry;
import com.haiminov.sunshine.databinding.FragmentDetailBinding;
import com.haiminov.sunshine.di.Injectable;
import com.haiminov.sunshine.di.SunshineViewModelFactory;
import com.haiminov.sunshine.model.DetailModel;
import com.haiminov.sunshine.viewmodel.DetailViewModel;

import javax.inject.Inject;

/**
 * A placeholder fragment containing a simple view.
 */
public class DetailFragment extends Fragment implements Injectable {

    private static final String FORECAST_SHARE_HASHTAG = " #SunshineApp";
    private ShareActionProvider mShareActionProvider;
    public static final String DETAIL_URI = "DF_URI_KEY";
    private DetailViewModel viewModel;
    @Inject
    DetailModel detailModel;
    @Inject
    SunshineViewModelFactory factory;

    //Loading from database -> demonstrating bindings

    public DetailFragment() {
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        detailModel.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        detailModel.onStop();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentDetailBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_detail, container, false);
        viewModel = ViewModelProviders.of(this, factory).get(DetailViewModel.class);
        binding.setViewModel(viewModel);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            mUri = arguments.getParcelable(DETAIL_URI);
            if (mUri == null) {
                throw new IllegalArgumentException("should pass in forecast uri");
            }
            detailModel.updateUri(mUri);
        }
        if (savedInstanceState == null) {
            viewModel.init();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.detailfragment, menu);
        MenuItem menuItem = menu.findItem(R.id.action_share);

        mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(menuItem);
        mShareActionProvider.setShareIntent(createShareForecastIntent());
    }

    private Intent createShareForecastIntent() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        shareIntent.setType("text/plain");
        if (viewModel != null) {
            shareIntent.putExtra(Intent.EXTRA_TEXT,
                    viewModel.forecastStr.get() + FORECAST_SHARE_HASHTAG);
        }
        return shareIntent;
    }

    private Uri mUri;

    void onLocationChanged( String newLocation ) {
        Uri uri = mUri;
        if (null != uri) {
            long date = WeatherContract.WeatherEntry.getDateFromUri(uri);
            mUri = WeatherEntry.buildWeatherLocationWithDate(newLocation, date);
            if (mUri != null) {
                detailModel.updateUri(uri);
            }
        }
    }
}